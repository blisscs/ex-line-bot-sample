FROM elixir:1.6.6-alpine
RUN mix local.hex --force
RUN mix local.rebar --force
RUN mkdir -p /ex-line-bot-sample
RUN rm -rf _build/*
RUN rm -rf deps/*
RUN rm -rf assets/node_modules
RUN rm -rf assets/elm-stuff
ADD . /ex-line-bot-sample
WORKDIR /ex-line-bot-sample
EXPOSE 4000
RUN MIX_ENV=prod mix deps.get
CMD MIX_ENV=prod PORT=4000 mix phx.server
