module View exposing (..)

import Model exposing (..)
import Msg exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)


view : Model -> Html Msg
view model =
    div [ class "container-fluid" ]
        [ div [ class "row" ]
            [ div [ class "col-10", id "event-log" ]
                ([ h2 [] [ text "Event Log" ]
                 ]
                    ++ List.map display_log model.events
                )
            , div [ class "col-2" ]
                [ h2 [] [ text "Barcode" ]
                , img [ src model.barcode_url, class "img-fluid" ] []
                ]
            ]
        , div [ class "row" ]
            [ div [ class "col" ]
                [ h2 [] [ text "Chat Windows" ] ]
            ]
        ]


display_log : Event -> Html Msg
display_log event =
    p [] [ text event.event_type, text " - ", text event.message ]
