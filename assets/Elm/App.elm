module App exposing (main)

import Html exposing (programWithFlags)
import View exposing (..)
import Model exposing (..)
import Msg exposing (..)
import Phoenix
import Phoenix.Socket as Socket
import Phoenix.Channel as Channel
import Json.Decode as JD


type alias Flags =
    { barcode_url : String
    , websocket_url : String
    }


main : Program Flags Model Msg
main =
    programWithFlags ({ init = init, update = update, subscriptions = subscriptions, view = view })


initialModel : Model
initialModel =
    { barcode_url = ""
    , websocket_url = ""
    , events = []
    }


init : Flags -> ( Model, Cmd Msg )
init flags =
    let
        model =
            { initialModel
                | barcode_url = flags.barcode_url
                , websocket_url = flags.websocket_url
            }
    in
        ( model, Cmd.none )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SUCCESSFULLYJOINEDEVENTLOG event ->
            ( model, Cmd.none )

        WEBHOOKREQUESTARRIVED event_response ->
            let
                result_event =
                    JD.decodeValue eventDecoder event_response

                event =
                    case result_event of
                        Ok event ->
                            event

                        Err _ ->
                            Debug.crash "Fail to decode event response from server"

                events =
                    event :: model.events
            in
                ( { model | events = events }, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions model =
    let
        socket =
            Socket.init model.websocket_url

        channel =
            Channel.init "rooms:event_log"
                |> Channel.onJoin (SUCCESSFULLYJOINEDEVENTLOG)
                |> Channel.on "webhook_request_arrived" (WEBHOOKREQUESTARRIVED)
    in
        Sub.batch [ Phoenix.connect socket [ channel ] ]
