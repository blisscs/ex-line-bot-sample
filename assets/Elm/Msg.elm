module Msg exposing (..)

import Json.Encode as JE


type Msg
    = SUCCESSFULLYJOINEDEVENTLOG JE.Value
    | WEBHOOKREQUESTARRIVED JE.Value
