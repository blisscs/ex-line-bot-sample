module Model exposing (..)

import Json.Decode exposing (string, Decoder, list)
import Json.Decode.Pipeline exposing (decode)


type alias Model =
    { barcode_url : String
    , websocket_url : String
    , events : List Event
    }


type alias Event =
    { event_type : String
    , message : String
    }


eventDecoder : Decoder Event
eventDecoder =
    decode Event
        |> Json.Decode.Pipeline.required "event_type" string
        |> Json.Decode.Pipeline.required "message" string
