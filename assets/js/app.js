import css from "../css/app.scss"

const Elm = require("../Elm/App.elm");
const mountNode = document.getElementById("elm-wrapper");

const ElmApp = Elm.App.embed(mountNode, {barcode_url: mountNode.dataset.barcode_url, websocket_url: mountNode.dataset.websocket_url})
