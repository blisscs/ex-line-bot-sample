defmodule ExLineBotSampleWeb.EventLogChannel do
  use Phoenix.Channel

  def join("rooms:event_log", _message, socket) do
    {:ok, socket}
  end
end
