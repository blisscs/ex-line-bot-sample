defmodule ExLineBotSampleWeb.SetRawData do
  alias Plug.Conn

  def check(%Conn{} = conn) do
    conn.req_headers
    |> Enum.any?(fn {header, _content} -> header == "x-line-signature" end)
  end
end
