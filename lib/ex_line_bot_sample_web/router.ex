defmodule ExLineBotSampleWeb.Router do
  use ExLineBotSampleWeb, :router

  alias ExLineBotSampleWeb.BroadcastEventPlug
  alias ExLineBotSampleWeb.VerifyPlug

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  pipeline :hook do
    plug(VerifyPlug)
    plug(BroadcastEventPlug)
  end

  scope "/", ExLineBotSampleWeb do
    # Use the default browser stack
    pipe_through(:browser)

    get("/", PageController, :index)
  end

  # Other scopes may use custom stacks.
  # scope "/api", ExLineBotSampleWeb do
  #   pipe_through :api
  # end

  scope "/webhook", ExLineBotSampleWeb do
    pipe_through(:hook)

    post("/", WebHookController, :index)
  end
end
