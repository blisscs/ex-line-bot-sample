defmodule ExLineBotSampleWeb.PageController do
  use ExLineBotSampleWeb, :controller

  @barcode_url System.get_env("BARCODE_URL")

  def index(conn, _params) do
    render(conn, "index.html", %{barcode_url: @barcode_url})
  end
end
