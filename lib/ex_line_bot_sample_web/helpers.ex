defmodule ExLineBotSampleWeb.Helpers do
  import ExLineBotSampleWeb.Router.Helpers

  alias ExLineBotSampleWeb.Endpoint

  def bot_socket_url do
    url = page_url(Endpoint, :index)

    "#{Regex.replace(~r/^(http)/, url, "ws")}socket/websocket"
  end
end
