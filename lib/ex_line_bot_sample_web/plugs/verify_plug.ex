defmodule ExLineBotSampleWeb.VerifyPlug do
  import Plug.Conn

  alias ExLineBotSample.Line.Client
  alias Plug.Conn

  def init(options) do
    # initialize options

    options
  end

  def call(%Conn{} = conn, _) do
    case Client.verify(conn) do
      {:ok, _} ->
        conn

      {:error, msg} ->
        conn
        |> send_resp(401, msg)
        |> halt()
    end
  end
end
