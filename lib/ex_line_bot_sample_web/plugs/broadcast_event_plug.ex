defmodule ExLineBotSampleWeb.BroadcastEventPlug do
  alias ExLineBotSample.Account
  alias ExLineBotSample.Account.User
  alias ExLineBotSampleWeb.Endpoint
  alias Plug.Conn

  def init(options) do
    # initialize options

    options
  end

  def call(%Conn{} = conn, _opt) do
    event = wipe_credential_and_do_action(conn.params)

    Endpoint.broadcast!("rooms:event_log", "webhook_request_arrived", event)

    conn
  end

  defp wipe_credential_and_do_action(
         %{
           "events" => [
             %{
               "source" => %{
                 "type" => "user",
                 "userId" => line_id
               },
               "type" => "follow"
             }
           ]
         } = _follow_params
       ) do
    {:ok, %User{} = user} = Account.upsert_user(line_id)

    %{event_type: "Follow Bot", message: "#{user.id}-#{user.fake_name} Follow Our Bot"}
  end

  defp wipe_credential_and_do_action(
         %{
           "events" => [
             %{
               "source" => %{
                 "type" => "user",
                 "userId" => line_id
               },
               "type" => "unfollow"
             }
           ]
         } = _unfollow_params
       ) do
    {:ok, %User{} = user} = Account.upsert_user(line_id)

    Account.delete_user!(user)

    %{event_type: "Unfollow Bot", message: "#{user.id}-#{user.fake_name} Unfollow Our Bot"}
  end

  defp wipe_credential_and_do_action(%{
         "events" => [%{"message" => %{"id" => "100001"}}, %{"message" => %{"id" => "100002"}}]
       }) do
    %{event_type: "WebHook Verification", message: "Line successfully verified our webook url."}
  end

  defp wipe_credential_and_do_action(_) do
    %{
      event_type: "Unrecognized!",
      message: "Could not recognized the request to the webhook!"
    }
  end
end
