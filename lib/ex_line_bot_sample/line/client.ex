defmodule ExLineBotSample.Line.Client do
  @channel_access_token Application.get_env(:ex_line_bot_sample, :line_channel_access_token)
  @channel_secret Application.get_env(:ex_line_bot_sample, :line_channel_secret)

  use ExLineBotSdk.LineClient
end
