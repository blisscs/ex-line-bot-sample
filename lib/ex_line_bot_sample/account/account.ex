defmodule ExLineBotSample.Account do
  @moduledoc """
  Account Context
  """

  alias ExLineBotSample.Account.User
  alias ExLineBotSample.Repo
  alias Faker.Name

  @doc """
  Delete user.
  raises exception if user doesn't exist

  Example:
      iex> delete_user!(%User{})
      %User{}
  """
  def delete_user!(%User{} = user) do
    Repo.delete!(user)
  end

  @doc """
  Insert or Update user given line_id.

  Example:
     iex> upsert_user("line_id")
     {:ok, %User{%{id: "[some_uuid]", line_id: "line_id"  ,fake_name: "[somefake name]"}}}
  """
  def upsert_user(line_id) do
    case get_user_by(%{line_id: line_id}) do
      %User{} = user ->
        {:ok, user}

      nil ->
        insert_user(line_id)
    end
  end

  @doc """
  Get user by id
  If found return %User{}
  If not return nil
  """
  def get_user(id) do
    User
    |> Repo.get(id)
  end

  @doc """
  Get user by a map
  If found return %User{}
  If not return nil
  """
  def get_user_by(map) do
    User
    |> Repo.get_by(map)
  end

  defp insert_user(line_id) do
    params = %{"line_id" => line_id, "fake_name" => Name.name()}

    %User{}
    |> User.changeset(params)
    |> Repo.insert()
  end
end
