defmodule ExLineBotSample.Account.User do
  use ExLineBotSample.Schema

  alias ExLineBotSample.Account.User

  import Ecto.Changeset

  schema "users" do
    field(:line_id)
    field(:fake_name)

    timestamps()
  end

  def changeset(%User{} = user, params) do
    user
    |> cast(params, [:line_id, :fake_name])
    |> validate_required([:line_id, :fake_name])
    |> unique_constraint(:line_id)
  end
end
