defmodule ExLineBotSampleWeb.PageControllerTest do
  use ExLineBotSampleWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Hello ExLineBotSample!"
  end
end
