defmodule ExLineBotSampleWeb.WebHookControllerTest do
  use ExLineBotSampleWeb.ConnCase

  alias ExLineBotSample.Account
  alias ExLineBotSample.Account.User

  @line_id "234234234234234234"

  setup do
    {:ok, %User{} = user} = Account.upsert_user(@line_id)

    {:ok, [user: user]}
  end

  test "must have conn.private[:raw_request_data] present in all webhook request(when x-line-signature exists)" do
    payload = "{ \"hello\": \"world\" }"

    conn =
      build_conn()
      |> put_req_header("x-line-signature", "somedummysignature")
      |> put_req_header("content-type", "application/json")
      |> post("/webhook", payload)

    assert conn.private[:raw_request_data] == payload
  end

  test "must not have conn.private[:raw_request_data] available when x-line-signature does not exists" do
    payload = "{ \"hello\": \"world\" }"

    conn =
      build_conn()
      |> put_req_header("content-type", "application/json")
      |> post("/webhook", payload)

    assert conn.private[:raw_request_data] == nil
  end

  test "Post /webhook with line verification" do
    payload =
      "{\n  \"events\": [\n    {\n      \"replyToken\": \"00000000000000000000000000000000\",\n      \"type\": \"message\",\n      \"timestamp\": 1531719983967,\n      \"source\": {\n        \"type\": \"user\",\n        \"userId\": \"Udeadbeefdeadbeefdeadbeefdeadbeef\"\n      },\n      \"message\": {\n        \"id\": \"100001\",\n        \"type\": \"text\",\n        \"text\": \"Hello, world\"\n      }\n    },\n    {\n      \"replyToken\": \"ffffffffffffffffffffffffffffffff\",\n      \"type\": \"message\",\n      \"timestamp\": 1531719983967,\n      \"source\": {\n        \"type\": \"user\",\n        \"userId\": \"Udeadbeefdeadbeefdeadbeefdeadbeef\"\n      },\n      \"message\": {\n        \"id\": \"100002\",\n        \"type\": \"sticker\",\n        \"packageId\": \"1\",\n        \"stickerId\": \"1\"\n      }\n    }\n  ]\n}\n"

    conn =
      build_conn()
      |> put_req_header("content-type", "application/json;charset=UTF-8")
      |> put_req_header("x-line-signature", "ZgHQTB1CE+MJSTRjsYaCq4mCTlNTpJHh8x86ykcSvYg=")
      |> post("/webhook", payload)

    assert conn.private[:raw_request_data] == payload

    assert text_response(conn, 200)
  end

  test "Post /webhook with follow event", %{conn: _conn, user: user} do
    @endpoint.subscribe("rooms:event_log")

    payload = """
    {
      "events": [
        {
          "replyToken": "sdfsafsdafsaf",
          "source": {
            "type": "user",
            "userId": "234234234234234234" 
          },
          "timestamp": 1530251955540,
          "type": "follow"
        }
      ]
    }
    """

    conn =
      build_conn()
      |> put_req_header("x-line-signature", "05XQqNYo0BGckqXGGHcZYHeWMjScsIaT2VtJwIiKcVg=")
      |> put_req_header("content-type", "application/json")
      |> post("/webhook", payload)

    assert text_response(conn, 200)

    assert_receive %Phoenix.Socket.Broadcast{
      event: "webhook_request_arrived",
      topic: "rooms:event_log",
      payload: received_payload
    }

    assert conn.private[:raw_request_data] == payload

    assert received_payload == %{
             event_type: "Follow Bot",
             message: "#{user.id}-#{user.fake_name} Follow Our Bot"
           }
  end

  test "Post /webhook with unfollow event", %{conn: _conn, user: user} do
    @endpoint.subscribe("rooms:event_log")

    payload = """
    {
      "events": [
        {
          "source": {
            "type": "user",
            "userId": "234234234234234234"
          },
          "timestamp": 1530251955540,
          "type": "unfollow"
        }
      ]
    }
    """

    conn =
      build_conn()
      |> put_req_header("x-line-signature", "uTJw5703mp2Oc0W+Pr9dpHiqPaevOh1/8+rRftbARws=")
      |> put_req_header("content-type", "application/json")
      |> post("/webhook", payload)

    assert conn.private[:raw_request_data] == payload

    assert_receive %Phoenix.Socket.Broadcast{
      event: "webhook_request_arrived",
      topic: "rooms:event_log",
      payload: received_payload
    }

    assert received_payload == %{
             event_type: "Unfollow Bot",
             message: "#{user.id}-#{user.fake_name} Unfollow Our Bot"
           }

    assert text_response(conn, 200)

    assert nil == Account.get_user(user.id)
  end
end
