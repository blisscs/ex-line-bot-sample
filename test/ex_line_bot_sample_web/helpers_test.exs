defmodule ExLineBotSampleWeb.HelpersTest do
  use ExLineBotSampleWeb.ConnCase

  import ExLineBotSampleWeb.Helpers

  test "bot_socket_url/1" do
    assert bot_socket_url() == "ws://localhost:4001/socket/websocket"
  end
end
