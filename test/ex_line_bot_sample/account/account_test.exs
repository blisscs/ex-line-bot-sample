defmodule ExLineBotSample.AccountTest do
  use ExLineBotSample.DataCase

  alias ExLineBotSample.Account
  alias ExLineBotSample.Account.User
  alias Ecto.UUID

  @line_id "242342423432423"

  test "upsert_user/1 with new line_id" do
    assert {:ok, %User{} = user} = Account.upsert_user(@line_id)

    assert user.line_id == @line_id
  end

  setup do
    {:ok, %User{} = user} = Account.upsert_user(@line_id)

    {:ok, [user: user]}
  end

  test "upsert_user/1 should return a previous user if that line_id already exists in DB", %{
    user: user
  } do
    {:ok, %User{} = returned_user} = Account.upsert_user(@line_id)

    assert returned_user == user
  end

  test "get_user/1 should return the user if exists", %{user: user} do
    retrieved_user = Account.get_user(user.id)

    assert retrieved_user.id == user.id
  end

  test "get_user/1 should return nil if that user does not exist" do
    assert nil == Account.get_user(UUID.generate())
  end

  test "delete_user!/1 should delete a user", %{user: user} do
    assert %User{} = Account.delete_user!(user)

    assert nil == Account.get_user(user.id)
  end
end
