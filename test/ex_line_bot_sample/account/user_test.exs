defmodule ExLineBotSample.Account.UserTest do
  use ExLineBotSample.DataCase

  alias ExLineBotSample.Account.User

  @valid_attrs %{
    "line_id" => "24234234234242342",
    "fake_name" => "John Doe"
  }

  @invalid_attrs %{
    "line_id" => "",
    "fake_name" => ""
  }

  describe "Users" do
    test "valid user" do
      changeset = User.changeset(%User{}, @valid_attrs)

      assert changeset.valid?
    end

    test "invalid user" do
      changeset = User.changeset(%User{}, @invalid_attrs)

      refute changeset.valid?

      assert %{line_id: ["can't be blank"], fake_name: ["can't be blank"]} = errors_on(changeset)
    end
  end
end
