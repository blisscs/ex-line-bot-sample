defmodule ExLineBotSample.Line.ClientTest do
  use ExLineBotSampleWeb.ConnCase

  alias ExLineBotSample.Line.Client

  test "must have conn.private[:raw_request_data] present in all webhook request(when x-line-signature exists)" do
    payload = "{ \"hello\": \"world\" }"

    conn =
      build_conn()
      |> put_req_header("x-line-signature", "R4ijDU6G9pjENk6WltpJYvRpBzRfYT971uAES+ucl3c=")
      |> put_req_header("content-type", "application/json")
      |> post("/webhook", payload)

    assert {:ok, conn} == Client.verify(conn)
  end
end
