defmodule ExLineBotSample.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    create table("users", primary_key: false) do
      add(:id, :uuid, primary_key: true)
      add(:line_id, :string, null: false)
      add(:fake_name, :string, null: false)
      timestamps()
    end

    create(unique_index("users", [:line_id]))
  end
end
