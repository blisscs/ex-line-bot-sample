use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :ex_line_bot_sample, ExLineBotSampleWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :ex_line_bot_sample, ExLineBotSample.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: System.get_env("DB_USER_TEST"),
  password: System.get_env("DB_PASSWORD_TEST"),
  database: System.get_env("DB_NAME_TEST"),
  hostname: System.get_env("DB_HOST_TEST"),
  port: System.get_env("DB_HOST_TEST_PORT"),
  pool: Ecto.Adapters.SQL.Sandbox

# Configure Line
config :ex_line_bot_sample,
  line_channel_access_token: System.get_env("LINE_CHANNEL_ACCESS_TOKEN_TEST"),
  line_channel_secret: System.get_env("LINE_CHANNEL_SECRET_TEST")
