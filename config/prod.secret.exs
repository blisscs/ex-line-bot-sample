use Mix.Config

# In this file, we keep production configuration that
# you'll likely want to automate and keep away from
# your version control system.
#
# You should document the content of this
# file or create a script for recreating it, since it's
# kept out of version control and might be hard to recover
# or recreate for your teammates (or yourself later on).
config :ex_line_bot_sample, ExLineBotSampleWeb.Endpoint,
  secret_key_base: "ubMHryJgUOSS9E07yfYfUErwGJj1v0V3TrBGCCZ2uIWCECZ4HuwdWiXz3Cb86Ot3"

# Configure your database
config :ex_line_bot_sample, ExLineBotSample.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: System.get_env("DB_USER_PROD"),
  password: System.get_env("DB_PASSWORD_PROD"),
  database: System.get_env("DB_NAME_PROD"),
  hostname: System.get_env("DB_HOST_PROD"),
  port: System.get_env("DB_HOST_PROD_PORT"),
  pool_size: 10

# Configure Line
config :ex_line_bot_sample,
  line_channel_access_token: System.get_env("LINE_CHANNEL_ACCESS_TOKEN"),
  line_channel_secret: System.get_env("LINE_CHANNEL_SECRET")
